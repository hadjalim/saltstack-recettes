adminitescia:
 user.present:
  - home: /home/adminitescia

AAAAB3NzaC1yc2EAAAABJQAAAQEAlHpoMGzAYa43hzvJs/P01I6y33g4un7gaMioESMnfZPtA7yQOnvXREwAR2BuDz27lh7jKhuQ8YT/UceEeae6xnjr2Qir+zackuRLC2i7VWoPVt6IY4uQmRIUfjymW2oKMP32w2NMTRKnZUj9nURJDZY/Bw7SQLGTp9yg69SuxaLhoeK/vf9TYZDesoN2Nh5ByT3WtZUO30wnsy33cuWszDPkfZlQI6xNIASyFEYDU7o6hJYYkFuUnBcRO3wZoJGbybrmVyXgJO5KgVAt+6brUqtrIKjZ92wq4UlGs2QJPH4YXFs030g4k59cQrADZFeA37feg9xBGV2wbLdiJv8wSw==: 
 ssh_auth.present:
  - user: adminitescia

mypkgs:
 pkg.installed:
  - pkgs:
    - htop
    - tmux
    - vim
    - zsh

salt-master:
 host.present: 
  - ip: 172.16.203.38 
