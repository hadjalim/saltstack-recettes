apache:
     pkg.installed:
      {% if grains['os'] == 'Ubuntu' %}
          - name: apache2
      {% elif grains['os'] == 'Debian' %}
          - name: apache2
      {% endif %}
     service.running:
          {% if grains['os'] == 'Ubuntu' %}
          - name: apache2
          {% elif grains['os'] == 'Debian' %}
          - name: apache2
          {% endif %}
          - require:
            - pkg: apache

fail2ban:
     pkg.installed:
      {% if grains['os'] == 'Ubuntu' %}
          - name: fail2ban
      {% elif grains['os'] == 'Debian' %}
          - name: fail2ban
      {% endif %}
     service.running:
          {% if grains['os'] == 'Ubuntu' %}
          - name: fail2ban
          {% elif grains['os'] == 'Debian' %}
          - name: fail2ban
          {% endif %}

